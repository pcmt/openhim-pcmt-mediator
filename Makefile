pcmt_image=pcmt/openhim-mediator:latest
current_dir=$(shell pwd)
OPENHIM_NETWORK ?= openhim-common_default

.PHONY: build
build:
	docker build -t ${pcmt_image} .

.PHONY: build-gitlab-base
build-gitlab-base:
	cd gitlab-base && \
	docker buildx build -t pcmt/openhim-mediator-gitlab-base .

.PHONY: dev
dev:
	docker-compose \
		-f docker-compose.yml \
		-f docker-compose.dev.yml \
		run \
		--service-ports \
		pcmt-mediator \
		watch-and-run

.PHONY: dev-cli
dev-cli:
	docker-compose \
		-f docker-compose.yml \
		-f docker-compose.dev.yml \
		run \
		--service-ports \
		--entrypoint "/bin/bash" \
		pcmt-mediator

.PHONY: test
test:
	docker-compose \
		-f docker-compose.yml \
		-f docker-compose.dev.yml \
		run \
		--rm \
		pcmt-mediator \
		test

.PHONY: ci-test
ci-test:
	docker-compose run --rm pcmt-mediator test

.PHONY: up
up:
	docker-compose up