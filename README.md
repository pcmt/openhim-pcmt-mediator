# openhim-pcmt-mediator

The PCMT mediator is intended to provide access to the product catalog,
hosted in PCMT, through _draft_ FHIR-like resources to an OpenHIE e-Health
Architecture, using the OpenHIM Mediator structure.

## requirements

1. Docker (w/ BuildKit)
1. Docker-compose
1. GNU Make

## quickstart

Jump to [production](#production) and [configuration](#configuration) if you
already have an OpenHIM and PCMT instance available to connect to, and don't
need to do development.

1. Get OpenHIM in docker containers: (skip if you have OpenHIM already)  
  `git clone https://github.com/jembi/openhim-common.git`
1. Start the Openhim: (skip if you have OpenHIM already)
    1. `cd openhim-common`
    2. (optional) if this will be remote to your computer, you can set the
        hostname by modifying the `host` value in `defaults.json`.
    3. `docker-compose up -d`
    4. Wait for the console to start, then login to `https://localhost:9000`,
        using `root@openhim.org` and `openhim-password`.
        - if using a self signed cert, accept it in your browser.
    5. On first login, it'll prompt to change the password, change it to
        `password`.  Alternatively change it to anything you wish, however tell
        the PCMT mediator what this is in the `src/index.ts` under
        `mediatorConfig`.
1. Get PCMT Mediator: (skip if you have PCMT already)
    1. `git clone https://gitlab.com/pcmt/openhim-pcmt-mediator.git`
    2. `cd openhim-pcmt-mediator`
    3. `export DOCKER_BUILDKIT=1` enable Docker's buildkit
    4. `make`
1. Start the PCMT Mediator:
    1. (skip if OpenHIM is on another computer) Check OpenHIM is in docker
        network `openhim-common_default` by running:
        `docker inspect -f "{{ .NetworkSettings.Networks }}" openhim-core`.
        If this is not the network, tell the mediator to use the network that
        `openhim-core`
        is in by setting `OPENHIM_NETWORK=<name of openhim network>`.
    2. (optional for insecure connections) if the connection between mediator
        and the OpenHIM is over the public internet, setup an
        [mTLS connection](doc/mtls-connection.md).
    3. `make up`

## development

1. `make`
2. First run:
   1. `make dev-cli`
   2. `yarn install`
3. `make dev-up`

See below Development section.

## production

`docker-compose up -d`

## Configuration

If the connection between mediator and the OpenHIM is over the public internet,
setup an [mTLS connection](doc/mtls-connection.md).

**NOTE**: Configuration, including the `openhimConfig.json`, and mTLS certs,
are kept in the `config/` directory.  When running the container, this folder
should be host mounted into the container path `/app/dist/config`.  When running
a `dev*` target, this will be host mounted to both `/app/config` as well.

## Logging

The log level can be set via environment:

```shell
logLevel=debug docker compose up -d`
```

The default level is `info`.

## Mode

The mediator is normally run by registering with an OpenHIM server, this mode is
the default `openhim`.

Optionally this mode may be skipped by environment, by setting:

```shell
mode=standalone docker compose up -d
```

In `standalone` mode, the PCMT instance may be configured, without OpenHIM, by
providing the credentials in `config/standalone.json`.  A template has been
provided in `config/standalone.json.dist` - copy this, fill it in, and remove
the `.dist`.

### OpenHIM Registration

You need:

1. Running OpenHIM, with ports accessible via network to mediator.
    - if using a self-signed TLS certificate, make sure [openhimConfig.json][openhimConfig]
    has `"trustSelfSigned": true`.
1. OpenHIM Admin credentials that can register a mediator.

Place these credentials into [openhimConfig.json][openhimConfig]. 

[openhimConfig]: config/openhimConfig.json

### PCMT API

You need:

1. Running PCMT, with ports accessible via network to mediator.
1. PCMT configured with [credentials for API access][akeneo_api].
1. Running OpenHIM and PCMT Mediator, [registered](#openhim-registration)

Place your PCMT API Credentials (click System -> API Connections):
![PCMT API Screen][pcmt-api-connection]

Into OpenHIM (click Mediators -> Gear Icon of PCMT Mediator: urn:mediator:pcmt-mediator)
![OpenHIM Mediator Configuration Screen][openhim-pcmt-mediator-config]

[akeneo_api]: https://help.akeneo.com/pim/v3/articles/manage-the-web-api-permissions.html
[openhim-pcmt-mediator-config]: doc/assets/openhim-pcmt-mediator-configuration.png
[pcmt-api-connection]: doc/assets/pcmt-api-connection.png

## OpenHIM Channels

There are 3 main channels.  For this document we will presume this is for a
National Product Catalog, and so each channel will be under the path `/npc/`.
Feel free to adjust as you see fit.

The channels:
- `/npc/secured` - Test if the Mediator and OpenHIM are connected securely. 
  A success message will return `mTLS Successful`.
- `/npc/gtin` - Return a CSV of the catalog keyed off of a single GTIN.
- `/npc/product` - Return a CSV of the catalog keyed off of each top-level
  Product.
- `/npc/marketAuthorization` - Returns a JSON array of the market authorizations,
   using a query parameter of either `?byGtin` or `?byProductId`.

## usage

To see the PCMT mediator working, we need:

1. Running PCMT, with [credentials for API access][akeneo_api].
1. Running OpenHIM, configured with PCMT credentials and 

If you're using Postman, download our API [Collection][postman-collection] and 
example environment, edit the [environment][postman-env] to fit your OpenHIM 
URI and credentials.

To test:

1. Open Postman
1. Import the above Collection and Environment
1. Edit the Environment, giving suitable values for each variable (URI, credentials, etc)
1. Open the Collection:  
      1. Run `/npc/secured`, success indicates a good, secure connection.
      2. Run `/npc/marketAuthorization?byGtin=someId` and  
       `/npc/marketAuthorization?byProductId=someId`, success is a HTTP 200 or 404.
      3. Run `/npc/product`, sucess is a CSV with > 1 row
      4. Run `/npc/gtin`, success is a CSV with > 1 row

CSV formats:
- mime type will be `text/csv`
- first row will be headers
- `,` will seperate rows
- Will use `[]` within a column to indicate multiple values, each seperated
  by `,`
- no data will return only the header row

[postman-collection]: doc/assets/OpenHIM-PCMT-Mediator.postman_collection.json
[postman-env]: doc/assets/PCMT-Mediator-Example.postman_environment.json

## development

In the `dev*` targets, the host filesystem is mounted into the container.

`make dev-cli` will give a shell in the container where further commands may be
run, such as `yarn add...`.

`make dev` will start the container and the internal node process, watching for
source changes and reloading as those occur.

## Deployment topology

See [deployment topology](doc/deployment-topology.md).

## Upgrade

Given a running mediator, you can upgrade:

1. `docker-compose pull`
1. `docker-compose up -d`

Check logs to see if successful:

`docker-compose logs -f`

And you should see:

```log
openhim-pcmt-mediator-pcmt-mediator-1  | 2022-12-09T20:44:07.597Z info: 	openhim config urn: urn:mediator:pcmt-mediator
openhim-pcmt-mediator-pcmt-mediator-1  | 2022-12-09T20:44:07.602Z info: 	Starting with mTLS...
openhim-pcmt-mediator-pcmt-mediator-1  | 2022-12-09T20:44:07.613Z info: 	Listening on port 3000
openhim-pcmt-mediator-pcmt-mediator-1  | 2022-12-09T20:44:08.815Z info: 	Successfully registered mediator
openhim-pcmt-mediator-pcmt-mediator-1  | 2022-12-09T20:44:08.816Z info: 	OpenHIM authentication successful
openhim-pcmt-mediator-pcmt-mediator-1  | 2022-12-09T20:44:09.967Z info: 	Initial mediator configuration successful
openhim-pcmt-mediator-pcmt-mediator-1  | 2022-12-09T20:44:09.968Z info: 	Setting new PCMT credentials for openhim.mediator
```

If successful, you may optionally remove old images with:

`docker image prune`