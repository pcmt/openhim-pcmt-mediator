#syntax=docker/dockerfile:1
FROM node:18

ADD . /app
WORKDIR /app

RUN <<EOF
yarn
yarn tsc
EOF

EXPOSE 3000
ENTRYPOINT ["yarn"]
CMD ["start"]