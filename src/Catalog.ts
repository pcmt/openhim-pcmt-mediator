import {Product, Item, ProductItemBase} from './fhir';
import log from './log';

const CHILD_NOT_FOUND = `Child entry not found in catalog, which indicates a 
    programming error as the catalog is constructed from children with parents`;

const PARENT_NOT_FOUND = `Parent entry not found in catalog, which indicates
    that a child references a parent not in the catalog, though it's also
    possibly a programming error`;

type ParentsAndChildren = {
    self: ProductItemBase,
    parents: Set<ProductItemBase>;
    children: Set<ProductItemBase>;
}

interface CatalogConstructor {
    products: Product[],
    items: Item[]
}

export default class Catalog {
    private products: Product[];
    private items: Item[];
    private parentChildMap: Map<string, ParentsAndChildren>;

    constructor(params: CatalogConstructor) {
        this.products = params.products;
        this.items = params.items;
        this.parentChildMap = new Map<string, ParentsAndChildren>();
        this.buildParentChildMap();
    }

    /**
     * Returns the top-most parents of a given child, excluding any intermediate
     * @param child the product to find parents of
     * @returns a set of unique parents, or an empty set.
     */
    public getTopParents(child?: ProductItemBase): Set<Product> {
        let topParents = new Set<Product>();
        if(!child) {
            for(let [id, parentsAndChildren] of this.parentChildMap) {
                //log.debug("in top parents " + id +", parents: " + parentsAndChildren.parents.forEach((p => {p.id})));
                if(0 == parentsAndChildren.parents.size) {
                    let asProduct = <Product> parentsAndChildren.self;
                    topParents.add(asProduct);
                }
            }
        } else {
            let parents = Array.from(this.getParents(child));
            while(0 < parents.length) {
                let parent = <ProductItemBase> parents.pop();
                let grandParents = Array.from(this.getParents(parent));
                if(0 == grandParents.length) topParents.add(<Product> parent);
                else parents.push(...grandParents);
            }
        }
        
        return topParents;
    };

    /**
     * Gets the grandkids, at the bottom of the heirarchy.
     * @param parent the parent for which we want the greatest known grandkids.
     * @returns the bottom of the heirarchy grandkids. 
     */
    public getBottomChildren(parent?: ProductItemBase): Set<ProductItemBase> {
        let bottomChildren = new Set<ProductItemBase>();
        if(!parent) {
            for(let [id, parentsAndChildren] of this.parentChildMap) {
                if(0 == parentsAndChildren.children.size) {
                    bottomChildren.add(parentsAndChildren.self);
                }
            }
        } else {
            let parentEntry = this.parentChildMap.get(parent.id);
            let children = Array.from(parentEntry?.children ?? []);
            while(0 < children.length) {
                let child = <ProductItemBase> children.pop();
                let grandChildren = Array.from(
                    this.parentChildMap.get(child.id)?.children ?? []);
                if(0 == grandChildren.length) bottomChildren.add(child);
                else children.push(...grandChildren);
            }
        }
        return bottomChildren;
    }

    /**
     * Gets from the build parent-child map this child's parents.
     * @param child the child we'd like the parent's of
     * @returns  the immediate parents of a given child
     */
    private getParents(child: ProductItemBase): Set<ProductItemBase> {
        if(null == child) return new Set<ProductItemBase>();
        return this.parentChildMap?.get(child.id)?.parents 
            ?? new Set<ProductItemBase>();
    }

    /**
     * Builds the parent-child map from all products and items
     */
    private buildParentChildMap() {
        let productsAndItems: ProductItemBase[] = [];
        productsAndItems.push(...this.products);
        productsAndItems.push(...this.items);

        // initalize map with self references
        for(let toAdd of productsAndItems) {
            this.addSelfToParentChildMap(toAdd);
        }
        
        // loop through all, for each entry add self as child for my parents
        for(let child of productsAndItems) {
            this.addChildrenToParentChildMap(child);
        }
        
        // loop through all, find each child entry, adding self to parent
        for(let parent of productsAndItems) {
            let children = this.parentChildMap.get(parent.id)?.children;
            children?.forEach( child => {
                let childsEntry = this.parentChildMap.get(child.id);
                if(childsEntry) {
                    childsEntry.parents.add(parent);
                } else {
                    log.error(CHILD_NOT_FOUND + ": Child: " + child);
                }
            });
        }
    }

    /**
     * Finds the parents of the entry to add, and adds that entry as a child of
     * them.
     * @param toAdd the entry to add as a child of its parents
     */
    private addChildrenToParentChildMap(toAdd: ProductItemBase) {
        // add toAdd as child of all it's parents
        let parentIds = this.findParentProductIds(toAdd);
        for(let parentId of parentIds) {
            let parentEntry = this.parentChildMap.get(parentId);
            if(parentEntry) {
                parentEntry.children.add(toAdd);
            } else {
                log.warn(PARENT_NOT_FOUND + ": Parent Id: " + parentId);
            }
        }
    }

    /**
     * Adds toAdd to parent and child map, initializing the entry if not 
     * initialized, ensuring self == toAdd.
     * @param toAdd entry to add
     * @returns the entry in the parent-child map, with toAdd, added.
     */
    private addSelfToParentChildMap(toAdd: ProductItemBase): ParentsAndChildren {
        let entry = this.parentChildMap.get(toAdd.id);
        if(!entry) {
            entry = {
                self: toAdd,
                parents: new Set<ProductItemBase>(),
                children: new Set<ProductItemBase>()
            }
        } else {
            entry.self = toAdd;
        }

        let returnMap = this.parentChildMap.set(toAdd.id, entry);
        return entry;
    }

    /**
     * Finds in FHIR the parent product association(s)
     * @param child an entry that might have parents
     * @returns array of parent ids, or empty array if none.
     */
    private findParentProductIds(child: ProductItemBase): string[] {
        let parentProductIds: string[] = [];
        if(child.association && child.association.associatedProduct) {
            // TODO:  when association in spec moves back to an array, will need 
            // to filter and map to find parents.
            if(child.association.associatedProduct.Product.type.toUpperCase() != 'PRODUCT') {
                log.error('Associated product is not parent');
                return parentProductIds;
            }
            if(child.association.associatedProduct.Product.display)
                parentProductIds.push(child.association.associatedProduct.Product.display);
        }

        return parentProductIds;

    }
}