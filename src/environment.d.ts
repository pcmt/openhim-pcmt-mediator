declare global {
    namespace NodeJS {
        interface ProcessEnv {
            logLevel: string
            mode: 'openhim' | 'standalone'
        }
    }
}
export {}