import express, {Request, Response, NextFunction} from 'express';
import {
    activateHeartbeat, authenticate, fetchConfig, registerMediator
} from 'openhim-mediator-utils';
import log from './log';
import mediatorConfig, { urn } from './mediatorConfig.json';
import openhimConfig from '../config/openhimConfig.json'
import {
    PcmtMediator,
    type PcmtCredentials,
    ProductData
} from './pcmt';
import CsvAdapter, {productsToCsv, gtinsToCsv, CMSTGtin, CMSTProduct} from './CsvAdapter';
import https from 'https';
import fs from 'fs';
import { TLSSocket } from 'tls';

initEnv();

// apply config
log.level = process.env.logLevel;
openhimConfig.urn = mediatorConfig.urn;
log.info("openhim config urn: " + openhimConfig.urn);

const app = express();
const port = 3000;
const pcmtMediator = new PcmtMediator(log);
const configPath = 'dist/config/';
const certPathsInConfig = {
    ca: configPath + 'ca-crt.pem',
    key: configPath + 'server-key.pem',
    cert: configPath + 'server-crt.pem'
}

// apply standalone config if mode set
if ('standalone' === process.env.mode) {
    log.info('Loading standalone configuration');
    const standaloneConfigPath = configPath + 'standaloneConfig.json';
    if(fs.existsSync(standaloneConfigPath)) {
        const contents = fs.readFileSync(standaloneConfigPath, 'utf-8');
        const asJson = JSON.parse(contents);

        pcmtMediator.setCredentials(asJson.pcmt)
    } else { 
        log.error("No standalone configuration found");
    }
}

// init defaults for env
function initEnv() {
    process.env.logLevel ??= 'info';
    log.info( 'ENV Log level: ' + process.env.logLevel );

    process.env.mode ??= 'openhim';
    log.info( 'ENV mode: ' + process.env.mode );
    switch(process.env.mode) {
        case 'openhim':
            log.info('OpenHIM mode');
            break;
        case 'standalone':
            log.info('Standalone mode');
            break;
        default:
            log.error('Mode not recognized: ' + process.env.mode);
            break;
    }
}

/* --- setup routes --- */

// set before other middleware operations: get, etc
if( hasCertsInConfig() ) app.use( clientAuthMiddleware ); 

app.get('/gtin', (req, res, next) => {
    log.debug("Getting gtin data");
    pcmtMediator.getData().then( (productData: ProductData) => {
        let asCMST = CsvAdapter(productData.products, productData.items);
        let gtinsCsv = gtinsToCsv(asCMST.gtin);
        res.attachment('gtins.csv').send(gtinsCsv);
    }).catch( err => {
        next(err);
    })
});

async function getByGtin(gtin: string): Promise<CMSTGtin | undefined> {
    let pcmtData = await pcmtMediator.getData();
    if( pcmtData ) {
        let asCMST = CsvAdapter(pcmtData.products, pcmtData.items);
        let foundGtin = asCMST.gtin.get(gtin);
        return foundGtin;
    }

    return undefined;
}

app.get('/gtin/:gtin', (req, res, next) => {
    const gtin = req.params.gtin;
    log.debug("gtin param: " + gtin);

    getByGtin(gtin).then( (foundGtin: CMSTGtin | undefined) => {
        if( foundGtin ) {
            foundGtin.marketingAuthorizations = [];
            res.json(foundGtin);
        } else {
            log.debug("Undefined gtin queried: " + gtin);
            res.sendStatus(404);
        }
    }).catch( err => {
        next(err);
    });
})

app.get('/product', (req, res, next) => {
    log.debug("Getting product data");
    pcmtMediator.getData().then( (productData: ProductData) => {
        let asCMST = CsvAdapter(productData.products, productData.items);
        let productsCsv = productsToCsv(asCMST.product);
        res.attachment('products.csv').send(productsCsv);
    }).catch( err => {
        next(err);
    })
});

async function getProductById(productId: string): Promise<CMSTProduct | undefined> {
    let pcmtData = await pcmtMediator.getData();
    if( pcmtData ) {
        let asCMST = CsvAdapter(pcmtData.products, pcmtData.items);
        let foundProduct = asCMST.product.get(productId);
        return foundProduct;
    }

    return undefined;
}

app.get('/product/:id', (req, res, next) => {
    const id = req.params.id;
    log.debug("Getting product data for id: " + id);
    
    getProductById(id).then( (foundProduct: CMSTProduct | undefined) => {
        if( foundProduct ) {
            foundProduct.marketingAuthorizations = [];
            res.json(foundProduct);
        } else {
            log.debug("Undefined product id reached: " + id);
            res.sendStatus(404);
        }
    }).catch( err => {
        next(err);
    });
});

app.get('/secured', (req, res) => {
    log.info('Secure endpoint reached');
    res.send('mTLS successful');
})

app.get('/marketAuthorization', (req, res, next) => {
    const gtin = <string>req.query?.byGtin;
    const productId = <string>req.query?.byProductId;
    if( !gtin && !productId ) {
        log.debug("Market auth queried without byGtin or byProductId");
        res.sendStatus(501); // tbi
    }
    
    let foundMarketAuths: string[] = [];
    if( gtin ) {
        log.debug("Gtin: " + gtin);
        getByGtin(gtin).then( (foundGtin: CMSTGtin | undefined) => {
            if( foundGtin ) {
                res.json(foundGtin.marketingAuthorizations);
            } else {
                res.sendStatus(404);
                log.debug("Undefined gtin for market auth queried: " + gtin);
            }
        }).catch( err => {
            next(err);
        });
    }

    if( productId ) {
        log.debug("Product id: " + productId);
        getProductById(productId).then( (foundProduct: CMSTProduct | undefined) => {
            if( foundProduct ) {
                res.json(foundProduct.marketingAuthorizations);
            } else {
                log.debug("Undefined product id for market auth queried: " + productId)
                res.sendStatus(404);
            };
        })
    }
});

app.all('*', (req, res, next) => {
    log.debug("Undefined route requested: " + req.path);
    res.sendStatus(404);
});

/* --- start http server --*/

if( hasCertsInConfig() ) {
    log.info("Starting with mTLS...")
    https.createServer(
        {
            ca: fs.readFileSync(certPathsInConfig.ca),
            key: fs.readFileSync(certPathsInConfig.key),
            cert: fs.readFileSync(certPathsInConfig.cert),
            requestCert: true,
            rejectUnauthorized: false
        },
        app
    ).listen(port);
} else {
    log.info("Certs not in config, starting without mTLS...")
    app.listen(port);
}

log.info(`Listening on port ${port}`);

/* --- register this mediator -- */
mediatorSetup();

/* --- functions --- */

function hasCertsInConfig(): boolean {
    return fs.existsSync(certPathsInConfig.ca)
        && fs.existsSync(certPathsInConfig.key)
        && fs.existsSync(certPathsInConfig.cert)
}

function getCertificate(tlsSock: TLSSocket): string {
    let peerCert = tlsSock.getPeerCertificate(true);
    if(peerCert && peerCert.raw) {
        return peerCert.raw.toString('base64');
    }

    return 'no peer certificate given';
}

function clientAuthMiddleware(req: Request, res: Response, next: NextFunction) {
    log.debug("Auth middleware reached");
    let tlsSock: TLSSocket = (req.socket as TLSSocket);
    log.debug('Peer certificate:  ' + getCertificate(tlsSock));
    
    if(tlsSock.authorized) {
        log.debug("Peer cert accepted!");
        return next();
    } else {
        log.warn('Peer Cert rejected: ' + tlsSock.authorizationError);
    }
}

type Configuration = {
    pcmt: PcmtCredentials
};

function mediatorSetup() {
    if( 'openhim' !== process.env.mode ) {
        log.info( 'Skipping openhim registration' );
        return;
    }
    registerMediator(openhimConfig, mediatorConfig, (err: any) => {
        if(err) {
            throw new Error(`Failed to register mediator, config: ${err}`);
        }
        log.info('Successfully registered mediator');

        const auth = authenticate(openhimConfig, (err: any) => {
            if (err) {
                throw new Error(`Failed to authenticate to OpenHIM: ${err}`);
            }
        })
        log.info(`OpenHIM authentication successful`);

        fetchConfig(openhimConfig, (err: any, initialConfig: Configuration) => {
            if(err) {
                throw new Error(`Failed to fetch initial config: ${err}`);
            }
            log.info('Initial mediator configuration successful');
            pcmtMediator.setCredentials(initialConfig.pcmt);
            
            const emitter = activateHeartbeat(openhimConfig);
            emitter.on('error', (err: any) => {
                log.error(`Heartbeat failed: ${err}`);
            })
            emitter.on('config', (newConfig: Configuration) => {
                log.info('Mediator configuration updated');
                pcmtMediator.setCredentials(newConfig.pcmt);
            })
        })
    })
}