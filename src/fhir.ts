export interface CodeableConcept {
    text: string
};

export interface Identifier {
    type: CodeableConcept,
    value: string
};

/*
 * Spec TODO: Clean up Description
 * 1. Should be the same across Product and Item
 * 2. Using a codeable concept (though not right in Product), would be
 *    more right, IFF a description was broken down into parts (e.g. strength 
 *    and dosage form)
 * 3. Using a locale (not language) and value pair feels right, IFF it could
 *    take multiple values (array).
 */
export interface ProductDescription {
    attributeTypeString: string
};

export interface ItemDescription {
    language: string,
    description: string
};

export interface ProductItemBase {
    resourceType: string,
    id: string,
    identifier?: Identifier[],
    association?: Association
};

export interface Product extends ProductItemBase {
    description: ProductDescription[],
};
export interface Association {
    associatedProduct: {
        Product: {
            type: string,
            display: string
        }
    }
}

export interface MarketingAuthorization {
    number: {
        value: string
    }
}

export interface Attributes {
    attributeType: {
        text: string
    },
    value: string
}

export interface Item extends ProductItemBase {
    description?: ItemDescription,
    marketingAuthorization?: MarketingAuthorization[],
    attributes?: Attributes[]
}
export interface PageLink {
    relation: string,
    url: string
};

export interface BundleEntry {
    resource: Product | Item,
    fullUrl: string
}

export type Bundle = {
    resourceType: string,
    type: string,
    link?: PageLink[],
    entry?: BundleEntry[]
};
