import winston, { transports, format } from 'winston';
const log = winston.createLogger({
    level: 'info',
    format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.align(),
        format.printf(info => `${info.timestamp} ${info.level}: ` + info.message)
    ),
    transports: [new transports.Console()]
});
export default log;