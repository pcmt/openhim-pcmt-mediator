import Catalog from './Catalog';
import { Product, Item, ProductItemBase } from './fhir';
import log from './log';

export interface CMSTProduct {
    productId: string,
    productDescriptions: string[],
    gtins: string[],
    manufacturers: string[],
    marketingAuthorizations: string[]
}

export const CMSTProductHeaders = [
    'productId',
    'productDescriptions'
    , 'gtins'
    , 'manufacturers'
    , 'marketingAuthorizations'
]

export interface CMSTGtin {
    gtin: string,
    description?: string,
    productIds: string[],
    marketingAuthorizations: string[]
}

export const CMSTGtinHeaders = [
    'gtin'
    , 'description'
    , 'productIds'
    , 'marketingAuthorizations'
]

export interface CMST {
    product: Map<string, CMSTProduct>,
    gtin: Map<string, CMSTGtin> 
}

export function transform(products: Product[], items: Item[]): CMST {
    let catalog = new Catalog({products: products, items: items});
    let asCMST: CMST = {
        product: toCMSTProducts(catalog),
        gtin: toCMSTGtins(catalog)
    };

    return asCMST;
}
export default transform;

function toCMSTProducts(catalog: Catalog): Map<string, CMSTProduct> {
    let asCMST = new Map<string, CMSTProduct>();
    for(let product of catalog.getTopParents()) {
        let items = catalog.getBottomChildren(product);
        let asCMSTProduct: CMSTProduct = {
            productId: product.id,
            productDescriptions: toProductDescriptions(product),
            gtins: toGtins(items),
            manufacturers: toManufacturers(items),
            marketingAuthorizations: toMarketAuthorizations(items)
        }
        asCMST.set(asCMSTProduct.productId, asCMSTProduct);
    }

    return asCMST;
}

function toCMSTGtins(catalog: Catalog): Map<string, CMSTGtin> {
    let gtins = new Map<string, CMSTGtin>();
    for(let item of catalog.getBottomChildren()) {
        let gtin = findGtin(item);
        if(gtin) {
            let parentProductIds = findProductIds(catalog.getTopParents(item));
            let marketAuths = findMarketingAuthorizations(item);
            let desc = toItemDescription(item);
            gtins.set( gtin,
                {
                    gtin: gtin,
                    description: desc,
                    productIds: parentProductIds,
                    marketingAuthorizations: marketAuths
                }
            );
        }
    }
    return gtins;
}

function findProductIds(products: Iterable<ProductItemBase>): string[] {
    let productIds: string[] = [];
    for(let product of products) {
        productIds.push(product.id);
    }

    return productIds;
}

// find all market auths of an Item, or empty array
function findMarketingAuthorizations(item: Item): string[] {
    let marketAuths: string[] = [];
    if(item.marketingAuthorization) {
        marketAuths = item.marketingAuthorization
            .map(marketAuth => marketAuth.number.value);
    }
    return marketAuths;
}

// find Gtin of Item
function findGtin(item: Item): string | undefined {
    if(item.identifier) {
        let gtinId = item.identifier.find(id => 
            id.type.text.toUpperCase() == 'GTIN');
        if(gtinId) return gtinId.value;

    }
    return undefined;
}

// find manufacturer name of Item
function findManufacturer(item: Item): string | undefined {
    if(item.attributes) {
        let manufacturerAtt = item.attributes.find(att => 
            att.attributeType.text.toUpperCase() == 'GS1_MANUFACTUREOFTRADEITEMPARTYNAME');
        if(manufacturerAtt) return manufacturerAtt.value;
    }
    return undefined;
}

// all gtins of all items
function toGtins(items: Set<Item> | undefined): string[] {
    let gtins: string[] = [];
    if(!items) return gtins;

    for(let item of items) {
        let gtin = findGtin(item);
        if(gtin) gtins.push(gtin);
    }
    return gtins;
}

// all manufacturers of all items
function toManufacturers(items: Set<Item> | undefined): string[] {
    let manufacturers: string[] = [];
    if(!items) return manufacturers;

    for(let item of items) {
        let mfg = findManufacturer(item);
        if(mfg) manufacturers.push(mfg);
    }

    return manufacturers;
}

// all market authorizations of all Items
function toMarketAuthorizations(items: Set<Item> | undefined): string[] {
    let marketAuths: string[] = [];
    if(!items) return marketAuths;

    for(let item of items) {
        marketAuths.push(...findMarketingAuthorizations(item));
    }

    return marketAuths;
}

function toProductDescriptions(product: Product): string[] {
    let productDescriptions: string[] = [];
    if(!product.description || Array.isArray(product.description) === false) 
        return productDescriptions;

    for(let desc of product.description) {
        let value = desc.attributeTypeString;
        if(value) productDescriptions.push(value);
    }
    return productDescriptions;
}

function toItemDescription(item: ProductItemBase): string {
    let itemDesc;
    let asItem = <Item> item;
    itemDesc = asItem.description?.description;
    itemDesc ??= '';
    return itemDesc;
}

export function gtinsToCsv(gtins: Map<string, CMSTGtin>): string {
    return rowsToCSV(gtins.values(), CMSTGtinHeaders);
}

export function productsToCsv(products: Map<string, CMSTProduct>): string {
    return rowsToCSV(products.values(), CMSTProductHeaders);
}

function jsonReplacer(key: any, value: any): any {
    if(Array.isArray(value) && value.length < 1) return undefined;
    return value;
}

function rowsToCSV<T>(data: Iterable<T>, keys: string[]): string {
    let asCsv = keys.join(',') + '\n';
    let rowKeys = keys as Array<keyof T>;
    for(let row of data) {
        let rowValues: string[] = [];
        for(let key of rowKeys) {
            rowValues.push(JSON.stringify(row[key], jsonReplacer));
        }
        asCsv += rowValues.join(',') + '\n';
    }

    return asCsv;
}