import transform, {CMSTGtin
    , CMSTGtinHeaders
    , CMSTProduct
    , CMSTProductHeaders
    , gtinsToCsv
    , productsToCsv} from '../CsvAdapter';
import {dummyItem, dummyMiddleProduct, dummyProduct} from './Catalog';
import log from '../log';

const expectedCMSTGtin: CMSTGtin = {
    gtin: <string> dummyItem.identifier?.[0].value,
    description: <string> dummyItem.description?.description,
    productIds: [<string> dummyProduct.id],
    marketingAuthorizations: [<string> dummyItem.marketingAuthorization?.[0].number.value]
}

const expectedCMSTProduct: CMSTProduct = {
    productId: dummyProduct.id,
    productDescriptions: [dummyProduct.description[0].attributeTypeString],
    gtins: [<string> dummyItem.identifier?.[0].value],
    manufacturers: [<string> dummyItem.attributes?.[0].value],
    marketingAuthorizations: [<string> dummyItem.marketingAuthorization?.[0].number.value]
}

test('no data', () => {
    let asCMST = transform([], []);
    expect(asCMST.gtin.size).toEqual(0);
    expect(asCMST.product.size).toEqual(0);
})

test('transform item', () => {
    let asCMST = transform([dummyProduct, dummyMiddleProduct], [dummyItem]);
    expect(asCMST.gtin.get(expectedCMSTGtin.gtin))
        .toEqual(expect.objectContaining(expectedCMSTGtin));
});

test('transform product', () => {
    let expectedCMSTProduct: CMSTProduct = {
        productId: dummyProduct.id,
        productDescriptions: [dummyProduct.description[0].attributeTypeString],
        gtins: [],
        manufacturers: [],
        marketingAuthorizations: []
    }

    let asCMST = transform([dummyProduct], []);
    expect(asCMST.product.get(dummyProduct.id)).toEqual(
        expect.objectContaining(expectedCMSTProduct));
});

test('test item and parent', () => {
    let asCMST = transform([dummyProduct, dummyMiddleProduct], [dummyItem]);
    expect(asCMST.product.get(dummyProduct.id))
        .toEqual(expect.objectContaining(expectedCMSTProduct));
    expect(asCMST.gtin.get(expectedCMSTGtin.gtin))
        .toEqual(expect.objectContaining(expectedCMSTGtin));
});

test('test gtin csv', () => {
    let gtin = expectedCMSTGtin.gtin;
    let desc = expectedCMSTGtin.description;
    let pIds = expectedCMSTGtin.productIds;
    let mAuths = expectedCMSTGtin.marketingAuthorizations;
    
    let expectedCsv = CMSTGtinHeaders.join(',') + '\n';
    expectedCsv += `"${gtin}","${desc}",["${pIds}"],["${mAuths}"]` + '\n';

    let gtinAsMap = new Map([
        [gtin, expectedCMSTGtin]
    ]);
    expect(gtinsToCsv(gtinAsMap)).toEqual(expectedCsv);
});

test('test gtin no data', () => {
    let gtin = expectedCMSTGtin.gtin;
    let testCMSTGtin: CMSTGtin = {
        gtin: gtin,
        description: undefined,
        productIds: [],
        marketingAuthorizations: []
    }

    let expectedCsv = CMSTGtinHeaders.join(',') + '\n';
    expectedCsv += `"${gtin}",,,` + '\n';

    let gtinAsMap = new Map([
        [gtin, testCMSTGtin]
    ]);
    expect(gtinsToCsv(gtinAsMap)).toEqual(expectedCsv);
});

test('test product csv', () => {
    let pId = expectedCMSTProduct.productId;
    let descs = expectedCMSTProduct.productDescriptions;
    let gtins = expectedCMSTProduct.gtins;
    let manus = expectedCMSTProduct.manufacturers;
    let mAuths = expectedCMSTProduct.marketingAuthorizations; 

    let expectedCsv = CMSTProductHeaders.join(',') + '\n';
    expectedCsv += `"${pId}",["${descs}"],["${gtins}"],["${manus}"],["${mAuths}"]` + '\n';

    let productAsMap = new Map([
        [pId, expectedCMSTProduct]
    ])
    expect(productsToCsv(productAsMap)).toEqual(expectedCsv);
});

test('test product no data', () => {
    let pId = expectedCMSTProduct.productId;
    let testProduct: CMSTProduct = {
        productId: pId,
        productDescriptions: [],
        gtins: [],
        manufacturers: [],
        marketingAuthorizations: []
    }

    let expectedCsv = CMSTProductHeaders.join(',') + '\n';
    expectedCsv += `"${pId}",,,,` + '\n';

    let productAsMap = new Map([
        [pId, testProduct]
    ])
    expect(productsToCsv(productAsMap)).toEqual(expectedCsv);
});