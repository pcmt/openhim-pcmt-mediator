import Catalog from '../Catalog';
import { Identifier, Item, Product } from '../fhir';

const dummyGtinIdentifier: Identifier = {
    type: {
        text: 'GTIN'
    },
    value: '012345678912'
};

export const dummyProduct: Product = { 
    id: 'dummyProduct',
    resourceType: 'product',
    description: [
        {
            attributeTypeString: 'some dummy product'
        }
    ]
};

export const dummyMiddleProduct: Product = {
    id: 'dummyMiddleProduct',
    resourceType: 'product',
    description: [
        {
            attributeTypeString: 'some dummy middle product'
        }
    ],
    association: {
        associatedProduct: {
            Product: {
                type: 'product',
                display: dummyProduct.id
            }
        }
    },
}

export const dummyItem: Item = {
    id: 'dummyItem',
    identifier: [
        dummyGtinIdentifier
    ],
    resourceType: 'item',
    description: {
        language: 'en_US',
        description: 'some dummy item'
    },
    association: {
        associatedProduct: {
            Product: {
                type: 'product',
                display: dummyMiddleProduct.id
            }
        }
    },
    marketingAuthorization: [
        {
            number: {
                value: 'someMarketAuthNumber'
            }
        }
    ],
    attributes: [
        {
            attributeType: {
                text: 'GS1_MANUFACTUREOFTRADEITEMPARTYNAME'
            },
            value: 'someValue'
        }
    ]
};

const dummyItem2: Item = {
    id: 'dummyItem2',
    resourceType: 'item',
    association: {
        associatedProduct: {
            Product: {
                type: 'product',
                display: dummyMiddleProduct.id
            }
        }
    }
};

const dummySideProduct: Product = {
    id: 'dummySideProductId',
    resourceType: 'Product',
    description: [
        {
            attributeTypeString: 'some side product'
        }
    ]
};

const dummySideItem: Item = {
    id: 'dummySideItemId',
    resourceType: 'Item',
    association: {
        associatedProduct: {
            Product: {
                type: 'product',
                display: dummySideProduct.id
            }
        }
    }
}

describe('top/bottom parents', () => {
    let catalog = new Catalog({
        products: [dummyProduct, dummyMiddleProduct, dummySideProduct]
        , items: [dummyItem, dummyItem2, dummySideItem]
    });
    
    test('get a top parent', () => {
        expect(catalog.getTopParents(dummyItem)).toContain(dummyProduct);
    });

    test('get all top parents', () => {
        let topParents = catalog.getTopParents();
        expect(topParents.size).toEqual(2);
        expect(topParents).toContain(dummyProduct);
        expect(topParents).toContain(dummySideProduct);
    });

    test('get a bottom child', () => {
        expect(catalog.getBottomChildren(dummySideProduct))
            .toContain(dummySideItem);
    });
    
    test('get all bottom children', () => {
        let bottomChildren = catalog.getBottomChildren();
        expect(bottomChildren.size).toEqual(3);
        expect(bottomChildren).toContain(dummyItem);
        expect(bottomChildren).toContain(dummyItem2);
        expect(bottomChildren).toContain(dummySideItem);
    });
})