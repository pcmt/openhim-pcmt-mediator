import axios, { AxiosResponse } from 'axios';
import {PcmtMediator} from '../pcmt';
import {
    Bundle
    , PageLink
} from '../fhir';
import log from '../log';
import MockAdapter from 'axios-mock-adapter';

const pageLinkSelf: PageLink[] = [
    {
        relation: "self",
        url: "https://self"
    },
    {
        relation: "next",
        url: "https://next"
    }
]

const pageLinkNext: PageLink[] = [
    {
        relation: "self",
        url: "https://next"
    }
]

const firstPageBundle: Bundle = {
    resourceType: "product",
    type: "search",
    link: pageLinkSelf,
    entry: []
}

const secondPageBundle: Bundle = {
    resourceType: "product",
    type: "search",
    link: [],
    entry: []
}

const mockAxios = new MockAdapter(axios);

describe('findNextPageLink', () => {
    test('should return next', async () => {
        expect(PcmtMediator.findUrlOfNextLink(pageLinkSelf))
        .toEqual('https://next');
    });

    test('should return undefined', async () => {
        expect(PcmtMediator.findUrlOfNextLink(pageLinkNext)).toEqual(undefined);
    });
});
