import axios from 'axios';
import { Logger } from 'winston';
import {Bundle, Item, PageLink, ProductItemBase, Product} from './fhir';

const apiProduct = '/api/fhir/products?limit=100';
const apiItem = '/api/fhir/items?limit=100';

export type PcmtCredentials = {
    uri?: string,
    username?: string,
    password?: string,
    client_id?: string,
    client_secret?: string
};

export interface ProductData {
    products: Product[],
    items: Item[]
};

export interface AuthCreds {
    access_token: string,
    refresh_token: string
};
export class PcmtMediator {
    private credentials: PcmtCredentials | undefined;
    private log: Logger;

    constructor(log: Logger) {
        this.log = log;
    }

    public setCredentials(credentials?: PcmtCredentials) {
        if(!credentials || Object.keys(credentials).length < 1) {
            this.log.error("Please set PCMT Credentials");
            return;
        }

        this.log.info("Setting new PCMT credentials for " + credentials.username);
        this.credentials = credentials;
    }

    public async getData(): Promise<ProductData> {
        if(!this.credentials) throw new Error('Credentials not set prior');
        try {
            let authToken = await this.authenticate();
            let products = await this.getWholeBundle(
                this.credentials.uri + apiProduct
                , authToken) as Product[];
            let items = await this.getWholeBundle(
                this.credentials.uri + apiItem
                , authToken) as Item[];
        
            this.log.info("Product Bundle length: " + products.length );
            this.log.info("Item bundle: " + items.length );
            return {products: products, items: items};
        } catch (error) {
            return {products: [], items: []};
        }
    }

    private async getWholeBundle(url: string, authToken: string)
        : Promise<ProductItemBase[]> 
    {
        const config = {
            headers: {
                Authorization: 'Bearer ' + authToken
            }
        };
        this.log.debug("requesting to: " + url);

        let bundleEntries: ProductItemBase[] = [];
        try {
            let pageUrl: string | undefined = url;
            while(pageUrl) {
                let res = await axios.get<Bundle>(pageUrl, config);
                let data = res.data;
                if(data.entry) {
                    let resources = data.entry.map(entry => entry.resource);
                    bundleEntries.push(...resources);
                }

                if(data.link) {
                    pageUrl = PcmtMediator.findUrlOfNextLink(data.link);
                } else {
                    pageUrl = undefined;
                }
            }
        } catch(error) {
            this.log.error("Error making request to: " + url);
            if(axios.isAxiosError(error)) {
                this.log.error("bad request: " + error.message);
            }
        }

        return bundleEntries;
    }

    public static findUrlOfNextLink(links: PageLink[]): string | undefined {
        if(!links) return undefined;

        let link = links.find( (link) => 
            link.relation.toLowerCase() === "next" );
        if (link) {
            return link.url;
        }
    }

    private async authenticate(): Promise<string> {    
        if(!this.credentials || Object.keys(this.credentials).length === 0) 
            throw new Error('PCMT credentials not yet set');

        const akeneoAuthBody = {
            username: this.credentials.username,
            password: this.credentials.password,
            grant_type: "password"
        };

        const akeneoAuthHeader = {
            Authorization: 'Basic ' + Buffer.from(
                this.credentials.client_id 
                + ':' + this.credentials.client_secret
                ).toString('base64')
        };

        let authToken="";
        try {
            let resp = await axios.post<AuthCreds>(
                this.credentials.uri + '/api/oauth/v1/token'
                , akeneoAuthBody
                , {headers: akeneoAuthHeader});
            this.log.info("PCMT Authentication success")
            authToken = resp.data.access_token;
            return authToken;
        } catch(error) {
            if (axios.isAxiosError(error) && error.response) {
                this.log.error(error.response.status);
                this.log.error(error.response.headers);
                this.log.error(error.response.config);
                this.log.error(error.message);
            } else {
                this.log.error("Something's gone wrong: " + error);
            }
            throw new Error("Authentication request failed"); //TODO: clean flow, add retry
        };
    };
};