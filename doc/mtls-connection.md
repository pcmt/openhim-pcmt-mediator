# Setting up an mTLS Connection

When the connection between the mediator and the OpenHIM occurs over the public
internet, it's a good idea to use an mTLS connection.  Such a connection
requires a bit of setup, however it helps mediate some vulnerable surfaces:

1. man-in-the-middle attacks
1. ensures the mediator can verify the identity of the OpenHIM
1. ensures the OpenHIM can verify the identity of the mediator
1. encrpyts traffic between the two

## Requirements for guide

1. General *nix and System Admin experience
1. OpenSSL
1. Shell access to PCMT Mediator
1. Web console access to OpenHIM

## Create the Certificates (and keys)

**NOTICE**: The following examples uses a `-days` argument to OpenSSL that will
generate certificates that will be good from now, till roughly 5 years from now.
You may want to use certificates with a shorter validity period for better
security.

> **NOTE**: It should be possible to use the `Certificates` section of 
> OpenHIM to generate self-signed certificates for both OpenHIM and the
> mediator, however at the time of this writing, this was experiencing
> issues with the OpenHIM server cert and the mediator cert being different.
> For this guide, we'll generate our own certificates, and we will use the
> OpenHIM Server cert both for the mediator, OpenHIM, and as a trusted
> OpenHIM certificate.  It's unclear if this is intentional or not.

Follow a guide for creating your own 
[keys and self-signed certificates][self-signed-certs].

### CA

```shell
openssl req -x509 -sha256 -days 1825 -nodes -newkey rsa:2048 -keyout ca.key -out ca.crt
```

Use sensible defaults for country name, common name, etc

### Mediator

You'll need to use the public FQDN for the mediator (or docker name if on a 
local docker network) for the Common Name.

> **NOTE**: Replace `Common Name` below, with the FQDN to easily track files`.

```shell
openssl req -newkey rsa:2048 -nodes -keyout common_name.key -out common_name.csr
openssl x509 -days 1825 -req -CA ca.crt -CAkey ca.key -in common_name.csr -out common_name.crt -CAcreateserial
```

You should now have:
- A certificate authority key and cert pair: `ca.key`, `ca.crt`.
- A Mediator key and cert (signed by CA) pair: `common_name.key`, `common_name.crt`.

[self-signed-certs]: https://www.baeldung.com/openssl-self-signed-cert

## Install in OpenHIM

See the project [OpenHIM Load CA](https://gitlab.com/pcmt/openhim-load-ca), with
the `ca.crt` you generated.

<details>

<summary>Old-manual method - not recommended</summary>

1. Go to the OpenHIM Certificates page.
1. Under server key and certificate, upload your generated `ca.key` and `ca.crt`.
1. Click the `Restart Now` button.
1. __wait for restart__
1. Return to Certificates page.
1. Under Trusted Certificates, upload `ca.crt`.

</details>

## Install in PCMT Mediator

1. Navigate to the PCMT Mediator's installation file system.
1. Copy your generated files to:
    - `common_name.key` -> `config/server-key.pem`
    - `common_name.crt` -> `config/server-crt.pem`
    - `ca.crt` -> `config/ca-crt.pem`
1. Restart container using `docker restart <container>`.