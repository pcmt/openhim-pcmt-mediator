# Deployment topology

Deploying the mediator has a couple options, each with different considerations.

### All together

In this deployment one server/machine hosts PCMT, the Mediator, and OpenHIM.
Such a deployment topology is more likely to be found in development/testing,
but _not_ in production.

Considerations:
- Port contention will be an issue, docker networking can help, however if
there is any public access either different ports will be mapped and exposed
outside the firewall, or an additional reverse proxy with v-hosts will be needed.
- Network and firewall rules might be simpler, though public access will
complicate things.
- Resource contention could impact availability to multiple components:  CPU,
memory, storage, etc.

### PCMT and OpenHIM (w/ Mediator)

In this deployment topology, there are 2 machines:  that which hosts the PCMT
and that which hosts the OpenHIM.  The mediator will be either be installed on
the PCMT machine, or the OpenHIM one.

Mediator on PCMT Host:

```plantuml
@startuml
skinparam componentStyle rectangle
skinparam linetype ortho

frame "PCMT Host" {
    component pcmt [
        PCMT

        Host the Product Catalog
    ]

    component mediator [
        PCMT Mediator

        OpenHIM Mediator to retrive product catalog, and transform it to
        downstream formats.
    ]

    [mediator] --> [pcmt]: REST (FHIR draft) for Product and Item
}

frame "OpenHIM Host" {
    component openhim [
        OpenHIM

        Interoperability Layer serves clients to access upstream resources
        through mediators.
    ]

    [mediator] --> [openhim]: (TLS) Self-register
    [mediator] <-- [openhim]: (mTLS) HTTP Routes
}
```

Mediator on OpenHIM Host:

```plantuml
@startuml
skinparam componentStyle rectangle
skinparam linetype ortho

frame "PCMT Host" {
    component pcmt [
        PCMT

        Host the Product Catalog
    ]
}

frame "OpenHIM Host" {
    component openhim [
        OpenHIM

        Interoperability Layer serves clients to access upstream resources
        through mediators.
    ]
    
    component mediator [
        PCMT Mediator

        OpenHIM Mediator to retrive product catalog, and transform it to
        downstream formats.
    ]

    [mediator] --> [pcmt]: (TLS) REST (FHIR draft) for Product and Item
    [mediator] --> [openhim]: Self-register
    [mediator] <-- [openhim]: HTTP Routes
}
```

Considerations:

- More (m)TLS connections to ensure security needed.
- If the mediator is hosted on the OpenHIM Host, then the PCMT or OpenHIM host
could become unavailable with fewer impacts on stakeholders.
- If the mediator is hosted on the PCMT host, the connection between
mediator and OpenHIM should use mTLS.
- Either PCMT or OpenHIM host will need more resources to handle mediator.
- Less resource contention between PCMT and OpenHIM.


### PCMT, OpenHIM, and Mediator (all seperate)

In this topology, each service has it's own host.

```plantuml
@startuml
skinparam componentStyle rectangle
skinparam linetype ortho

frame "PCMT Host" {
    component pcmt [
        PCMT

        Host the Product Catalog
    ]
}

frame "OpenHIM Host" {
    component openhim [
        OpenHIM

        Interoperability Layer serves clients to access upstream resources
        through mediators.
    ]
}

frame "Mediator Host" {
    component mediator [
        PCMT Mediator

        OpenHIM Mediator to retrive product catalog, and transform it to
        downstream formats.
    ]

    [mediator] --> [pcmt]: (TLS) REST (FHIR draft) for Product and Item
    [mediator] --> [openhim]: (TLS) Self-register
    [mediator] <-- [openhim]: (mTLS) HTTP Routes
}
```

Considerations:

- Least resource contetion between services.
- Requires the TLS and mTLS connections.
- More networking and sys-admin configuration.
- Best for higher-availability.
